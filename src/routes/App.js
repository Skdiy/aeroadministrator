import React from 'react'
import { Route, Router, Switch } from 'react-router-dom'
// Components
import PrivateRoute from '../AdministratorScope/Components/PrivateRouteContainer/PrivateRouteContainer'
import Layout from '../AdministratorScope/Layout/Layout'
// Admin Screens
import LoginAdministrator from '../AdministratorScope/Screens/Login/Login'
import GeneralRegister from '../AdministratorScope/Screens/Dashboard/Admin/GeneralRegister'
import Interviews from '../AdministratorScope/Screens/Dashboard/Admin/Interviews'
import AdminCourses from '../AdministratorScope/Screens/Dashboard/Admin/Courses'
import Articles from '../AdministratorScope/Screens/Dashboard/Admin/Articles'
import LiveStream from '../AdministratorScope/Screens/Dashboard/Admin/LiveStream'
import CoursesDetails from '../AdministratorScope/Screens/Dashboard/Admin/Courses/CoursesDetails'
// Home Screens
import Content from '../containers/app/Content'
import ContentIn from '../containers/app/ContentIn'
import CourseRealTime from '../containers/app/CourseRealTime'
import Courses from '../containers/app/Courses'
import CourseTR from '../containers/app/CourseTR'
import Index from '../containers/app/Index'
import LivePlane from '../containers/app/LivePlane'
// History
import history from './history'

const App = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Index} />
        <Route exact path="/curso-tiempo-real/:id" component={CourseRealTime} />
        <Route exact path="/curso-no-inscrito" component={CourseTR} />
        <Route exact path="/contenido" component={Content} />
        <Route exact path="/cursos" component={Courses} />
        <Route exact path="/contenido-interno" component={ContentIn} />
        <Route exact path="/live-programado" component={LivePlane} />

        {/* Administrator Routes */}
        <Route path="/admin/login" component={LoginAdministrator} />
        <Layout>
          <PrivateRoute
            path="/admin/general-register"
            component={GeneralRegister}
          />
          <PrivateRoute path="/admin/interviews" component={Interviews} />
          <PrivateRoute path="/admin/course/:id" component={CoursesDetails} />
          <PrivateRoute path="/admin/courses" component={AdminCourses} />
          <PrivateRoute path="/admin/articles" component={Articles} />
          <PrivateRoute path="/admin/live-streams" component={LiveStream} />
        </Layout>
      </Switch>
    </Router>
  )
}
export default App
