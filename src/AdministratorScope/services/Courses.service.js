import Swal from 'sweetalert2'
import api from './Api'

const handleError = () => {
  Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: 'Algo salio mal, vuelve a intentarlo luego',
  })
}

// TODO: Uncomment when needed
// const handleSuccess = () => {
//   Swal.fire({
//     icon: "success",
//     title: "Éxito",
//     text: "El registro fue realizado con éxito",
//   });
// };

const getCourses = () => {
  const response = api.get(`/course`).catch((error) => {
    console.log(error)
    handleError()
  })
  return response
}

export { getCourses }
