import React, { useState } from 'react'

// Components
import Loading from '../../../Components/Global/Loading/Loading'
import TitleSection from '../../../Components/Global/TitleSection/TitleSection'
import TableBusiness from '../../../Components/Tables/Admin/Education/Register/TableBusiness/TableBusiness'
import TableParticipants from '../../../Components/Tables/Admin/Education/Register/TableParticipants/TableParticipants'
import TableTeachers from '../../../Components/Tables/Admin/Education/Register/TableTeachers/TableTeachers'
import TabsNavigation from '../../../Components/Tabs/TabsNavigation/TabsNavigation'
// Custom Hooks
import useGeneralRegisterQuery from '../../../Utils/customHooks/useGeneralRegisterQuery'

function GeneralRegister() {
  const [value, setValue] = useState('Participantes')
  const { participantsList, teachersList } = useGeneralRegisterQuery()
  const { businessList, certificatesList } = useGeneralRegisterQuery()
  const { data, isLoading } = participantsList
  const { data: participantsData } = data ?? {}

  const labels = [
    'Participantes',
    'Profesores',
    'Empresas',
    'Certificados & diplomas',
  ]

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  // TODO: Pendiente tabla 'Certificados & Diplomas'
  const tables = () => {
    switch (value) {
      case 'Participantes':
        return <TableParticipants list={participantsData?.data} />
      case 'Profesores':
        return <TableTeachers list={teachersList?.data?.data?.data} />
      case 'Empresas':
        return <TableBusiness list={businessList?.data?.data?.data} />
      case 'Certificados & diplomas':
        return <div>Certificados{certificatesList?.data?.data?.data}</div>
      default:
        break
    }
  }

  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <TitleSection label={'Registro General'} />
          <TabsNavigation
            labels={labels}
            handleChange={handleChange}
            value={value}
          />
          {tables()}
        </>
      )}
    </>
  )
}

export default GeneralRegister
