/* Todo: Delete when using queryApi */
export const dataFake = {
  id: 1,
  name: 'Turbohélices',
  code: 'C12458',
  category: 'Upgrade',
  date_ini: '12 Marzo 2021',
  date_end: '31 Marzo 2021',
  n_inscritos: 10,
  capac: 10,
  teacher: 'Garrido A',
  state: ' Encurso',
  country: 'Perú',
  license: '525252 HYD',
  hours: 30,
  days: 12,
  mod: 'Online',
  clases: 15,
}

/* Todo: Delete when using queryApi */
export const alumnoCourse = [
  {
    name: 'Renzo Abanto',
    image: '',
    country: 'Perú',
    email: 'kevin@sector.aeronautico',
    course: 'A320',
    condition: false,
    state: 'En curso',
    progress: 75,
    total_asistencia: 15,
    asistencia: 8,
    test_01: 95,
    test_02: 0,
    proyect: 0,
    prom: 0,
    inasistencia: 1,
    total: 0,
  },
]

export const labels = [
  'INTEGRANTE',
  'COUNTRY',
  'CORREO',
  'CURSO',
  'CONDICIÓN',
  'Estado',
  'Avance%',
  'Asistencia',
  'test1',
  'test2',
  'PROYECT',
  'PROM',
  'INAST.',
  'P.TOTAL',
  'progreso',
]
