import React from 'react'

import { Avatar, Button, Chip } from '@mui/material'
import { Paper, Table, TableBody } from '@mui/material'
import { TableCell, TableContainer, TableRow } from '@mui/material'
// History - React Router
import history from '../../../../../routes/history'
// Components
import TitleSection from '../../../../Components/Global/TitleSection/TitleSection'
import TableCourseParticipantDetails from '../../../../Components/Global/Sidebar/SubTables/Admin/Details/TableCourseParticipantDetails'
// Config
import { variables } from '../../../../config/variables'
/*
  Todo: Eliminar cuando se haga el queryApi.
    endpoint: 'http://127.0.0.1:8080/api/course/1'
*/
// Helper
import { alumnoCourse, dataFake, labels } from './CoursesDetails.helper'
// Styles
import Styles from './CoursesDetails.styles'

const { palette } = variables

export default function CoursesDetails() {
  /* Todo: Eliminar cuando se haga uso del queryApi */
  const courseData = dataFake

  return (
    <Styles>
      <TitleSection label={`Mis Cursos`} />

      <div className="header">
        <p className="header__text">Cursos Online activos</p>
        <p
          className="header__text"
          onClick={() => history.push('/admin/courses')}
        >
          Regresar atras
        </p>
      </div>

      <div className="containerTable">
        <Paper className="container">
          <TableContainer>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableBody className="tableComponent__body">
                <TableRow
                  key={courseData.id}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  className="tableComponent__row"
                >
                  <TableCell className="tableComponent__text tableComponent__text--secondary">
                    <div className="tableComponent__body__user">
                      <Avatar
                        alt={`${courseData.name}`}
                        src={
                          courseData.image ||
                          'https://mui.com/static/images/avatar/1.jpg'
                        }
                        className="tableComponent__body__user__avatar"
                      />
                      <Button
                        variant="outlined"
                        className="tableComponent__button"
                      >
                        {courseData.name}
                      </Button>
                    </div>
                  </TableCell>
                  <TableCell className="tableComponent__text tableComponent__text--secondary">
                    <Button
                      variant="outlined"
                      className="tableComponent__button"
                    >
                      {/*TODO: code, no se especifica en el backend*/}
                      {courseData.code || 'C12458'}
                    </Button>
                  </TableCell>
                  <TableCell className="tableComponent__text tableComponent__text--secondary">
                    {courseData.level || 'Upgrade'}
                  </TableCell>
                  <TableCell className="tableComponent__text tableComponent__text--secondary">
                    {courseData.clases} clases en total
                  </TableCell>
                  <TableCell className="tableComponent__text tableComponent__text--secondary">
                    <Button
                      variant="outlined"
                      className="tableComponent__button"
                    >
                      {courseData.n_inscritos} alumnos inscritos
                    </Button>
                  </TableCell>
                  <TableCell className="tableComponent__text tableComponent__text--secondary">
                    A cargo de {courseData.teacher}
                  </TableCell>
                  <TableCell className="tableComponent__text tableComponent__text--secondary">
                    {/*TODO: Estado, no se especifica en el backend*/}
                    <Chip
                      sx={{
                        backgroundColor: courseData.status
                          ? palette.dark
                          : palette.white,
                        color: courseData.status ? palette.white : palette.neon,
                      }}
                      className="chip"
                      label={'En curso'}
                    />
                  </TableCell>
                  <TableCell className="tableComponent__text tableComponent__text--secondary">
                    <Button
                      variant="outlined"
                      className="tableComponent__button"
                    >
                      {/*TODO: Link a otro component*/}
                      {'Ver'}
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>

      <div className="containerData">
        <div className="containerData__box">
          <div className="containerData__wrapper">
            <h1 className="containerData__title">{courseData.name}</h1>
            <p className="containerData__code">Codigo: {courseData.code}</p>
          </div>
          <div className="wrapperData">
            <ul className="wrapperData__line">
              <li>Intructor: {courseData.teacher}</li>
              <li>Nacionalidad: {courseData.country}</li>
              <li>Licencia: {courseData.license} </li>
              <li>Cantidad de horas: {courseData.hours}</li>
              <li>Duración días: {courseData.days}</li>
              <li>Modalidad: {courseData.mod} </li>
              <li>Fecha de inicio: {courseData.date_ini}</li>
              <li>Fecha finalizacion: {courseData.date_end}</li>
            </ul>
          </div>
          {/* Tabla Participantes que llevan el curso */}
          <TableCourseParticipantDetails labels={labels} list={alumnoCourse} />

          <div className="wrapperButton">
            <Button className="wrapperButton__button">Descargar</Button>
          </div>
        </div>
      </div>
    </Styles>
  )
}
