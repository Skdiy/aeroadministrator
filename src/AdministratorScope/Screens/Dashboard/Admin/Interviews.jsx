import React, { useState } from 'react'

// Components
import TitleSection from '../../../Components/Global/TitleSection/TitleSection'
import TableAllInterviews from '../../../Components/Tables/Admin/Education/Interviews/TableAllInterviews/TableAllInterviews'
import TabsNavigation from '../../../Components/Tabs/TabsNavigation/TabsNavigation'

function Interviews() {
  // TODO: Cambiar servicios
  const [value, setValue] = useState('Todos entrevistas solicitadas')

  const labels = ['Todos entrevistas solicitadas', 'Programas', 'Empresas']

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  // TODO: Cambiar tablas
  const tables = () => {
    switch (value) {
      case 'Todos entrevistas solicitadas':
        return <TableAllInterviews list={[]} />
      case 'Programas':
        return <div>{value}</div>
      case 'Empresas':
        return <div>{value}</div>
      default:
        break
    }
  }

  return (
    /*   <>
      {isLoading ? (
        <Loading />
      ) : ( */
    <>
      <TitleSection label={'Entrevistas'} />
      <TabsNavigation
        labels={labels}
        handleChange={handleChange}
        value={value}
      />
      {tables()}
    </>
    /*   )}
    </> */
  )
}

export default Interviews
