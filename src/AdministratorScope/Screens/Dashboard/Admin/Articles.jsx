import React, { useState } from 'react'

// Components
import TitleSection from '../../../Components/Global/TitleSection/TitleSection'
import TableArticles from '../../../Components/Tables/Admin/Education/Articles/TableArticles/TableArticles'
import TabsNavigation from '../../../Components/Tabs/TabsNavigation/TabsNavigation'

export default function Articles() {
  const [value, setValue] = useState('Artículos por publicar')
  const labels = ['Artículos por publicar', 'Crear Artículos']

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  // TODO: Cambiar tablas
  const tables = () => {
    switch (value) {
      case 'Artículos por publicar':
        return <TableArticles list={[]} />
      case 'Crear Artículos':
        return <div>{value}</div>
      default:
        break
    }
  }

  return (
    /*   <>
        {isLoading ? (
          <Loading />
        ) : ( */
    <>
      <TitleSection label={'Articulos'} />
      <TabsNavigation
        labels={labels}
        handleChange={handleChange}
        value={value}
      />
      {tables()}
    </>
    /*   )}
    </> */
  )
}
