import React, { useState } from "react";
import CoursesForm from "../../../Components/CoursesForm/CoursesForm";

// Components
import Loading from "../../../Components/Global/Loading/Loading";
import TitleSection from "../../../Components/Global/TitleSection/TitleSection";
import TableCoursesOnline from "../../../Components/Tables/Admin/Education/Courses/TableCoursesOnline/TableCoursesOnline";
import TableProgam from "../../../Components/Tables/Admin/Education/Courses/TableProgram/TableProgram";
import TabsNavigation from "../../../Components/Tabs/TabsNavigation/TabsNavigation";
// Custom Hooks
import useGeneralCoursesQuery from "../../../Utils/customHooks/useCoursesQuery";

function Courses() {
  const [value, setValue] = useState("Cursos Online");
  const labels = ["Cursos Online", "Programas", "Crear curso / carrera"];
  const { coursesList } = useGeneralCoursesQuery();
  const { data, isLoading } = coursesList;
  const { data: coursesData } = data ?? {};

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // TODO: Cambiar tablas "Programas y Crear Curso/Carrera"
  const tables = () => {
    switch (value) {
      case "Cursos Online":
        return <TableCoursesOnline list={coursesData?.data} />;
      case "Programas":
        return <TableProgam list={coursesData?.data} />;
      case "Crear curso / carrera":
        return <CoursesForm />;
      default:
        break;
    }
  };

  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <TitleSection label={"Cursos"} />
          <TabsNavigation
            labels={labels}
            handleChange={handleChange}
            value={value}
          />
          {tables()}
        </>
      )}
    </>
  );
}

export default Courses;
