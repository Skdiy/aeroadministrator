import React, { useState } from 'react'

// Components
import TitleSection from '../../../Components/Global/TitleSection/TitleSection'
import TableLiveStream from '../../../Components/Tables/Admin/Education/LiveStream/TableLiveStream/TableLiveStream'
import TabsNavigation from '../../../Components/Tabs/TabsNavigation/TabsNavigation'

export default function LiveStream() {
  const [value, setValue] = useState('Livestream próximos')

  const labels = ['Livestream próximos', 'Crear Livestream']

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  // TODO: Cambiar tablas
  const tables = () => {
    switch (value) {
      case 'Livestream próximos':
        return <TableLiveStream list={[]} />
      case 'Crear Livestream':
        return <div>{value}</div>
      default:
        break
    }
  }

  return (
    /*   <>
        {isLoading ? (
          <Loading />
        ) : ( */
    <>
      <TitleSection label={'LiveStream'} />
      <TabsNavigation
        labels={labels}
        handleChange={handleChange}
        value={value}
      />
      {tables()}
    </>
    /*   )}
    </> */
  )
}
