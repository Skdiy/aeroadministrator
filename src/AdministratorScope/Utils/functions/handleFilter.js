/**
 * Funcion para filtrar un arreglo de objetos
 * @param {Object} property - JSON
 * @param {string} filter - keyword para filtrar
 * @returns True o False
 */
export const handleFilter = (property, filter) => {
  const { name, surname } = property
  return (
    name.toLowerCase().includes(filter.toLowerCase()) ||
    surname.toLowerCase().includes(filter.toLowerCase())
  )
}
