import { useQuery } from 'react-query'
// Constants
import { CONSTANTS } from '../../config/constants'
// Services
import { getCourses } from '../../services/Courses.service'

const { MIN_10 } = CONSTANTS

const useGeneralCoursesQuery = () => {
  const coursesList = useQuery(
    ['coursesList'],
    () => {
      return getCourses()
    },
    {
      enabled: true,
      staleTime: MIN_10,
    }
  )

  return { coursesList }
}

export default useGeneralCoursesQuery
