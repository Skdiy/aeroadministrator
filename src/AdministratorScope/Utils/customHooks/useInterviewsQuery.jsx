import { useQuery } from "react-query";

// Services
import { getCareer } from "../../services/Interviews.service";
import { getBusiness } from "../../services/Interviews.service";
// Constants
import { CONSTANTS } from "../../config/constants";

const { MIN_10 } = CONSTANTS;

const useGeneralRegister = () => {
  const CareerList = useQuery(
    ["businessList"],
    () => {
      return getCareer();
    },
    {
      enabled: true,
      staleTime: MIN_10,
    }
  );

  const businessList = useQuery(
    ["businessList"],
    () => {
      return getBusiness();
    },
    {
      enabled: true,
      staleTime: MIN_10,
    }
  );

  return { CareerList, businessList };
};

export default useGeneralRegister;
