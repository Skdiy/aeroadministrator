import React, { useState } from 'react'

import { Button } from '@mui/material'
import { Paper, Table, TableBody } from '@mui/material'
import { TableCell, TableContainer, TableHead, TableRow } from '@mui/material'
import { FaUserPlus } from 'react-icons/fa'
// Config
import CustomModal from '../../../../../Global/Modal/Modal'
// Components
import SearchBasic from '../../../../../Global/SearchBasic/SearchBasic'
import SelectBasic from '../../../../../Global/SelectBasic/SelectBasic'
// Helper
import { labels } from './TableAllInterviews.helper'
// Functions
import { handleFilter } from '../../../../../../Utils/functions/handleFilter'
import { handleDynamicSort } from '../../../../../../Utils/functions/handleDynamicSort'
// Styles
import Styles from './TablleAllInterviews.styles'

export default function TableAllInterviews(props) {
  const { list } = props
  const [orderArray, setOrderArray] = useState('')
  const [filterArray, setFilterArray] = useState('')

  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleGetInput = (value) => {
    setFilterArray(value)
  }

  const handleGetFilter = (value) => {
    setOrderArray(value)
  }

  return (
    <Styles>
      <div className="header">
        <SearchBasic
          label={'Buscar entrevistas'}
          handleGetInput={handleGetInput}
        />
        <SelectBasic labels={labels} handleGetFilter={handleGetFilter} />
      </div>
      <div className="header">
        <Button onClick={handleOpen} startIcon={<FaUserPlus />}>
          Crear cita
        </Button>
      </div>
      <div className="containerTable">
        <Paper className="container">
          <TableContainer component={Paper} className="tableComponent">
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead className="tableComponent__head">
                <TableRow
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  className="tableComponent__row"
                >
                  {labels.map(({ name, key }) => (
                    <TableCell
                      className="tableComponent__text"
                      key={key.length ? key : name}
                    >
                      {name}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody className="tableComponent__body">
                {list.length ? (
                  list
                    ?.filter((row) => handleFilter(row, filterArray))
                    ?.sort(handleDynamicSort(orderArray))
                    ?.map((row, index) => (
                      <TableRow
                        key={index}
                        sx={{
                          '&:last-child td, &:last-child th': { border: 0 },
                        }}
                        className="tableComponent__row"
                      >
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Código, no se especifica en el backend*/}
                          {/* Tal vez sea typeDoc */}
                          {row.typeDoc}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Tipo, es de tipo entero pero no sabemos que valores toma por defecto*/}
                          {row.type || 'Pilot private'}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {row.pais}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {row.names}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Mensaje, no se especifica en el backend*/}
                          Ver
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Estado, no se especifica en el backend*/}
                          {/* <Chip
                          sx={{
                            backgroundColor: row.status
                              ? palette.dark
                              : palette.white,
                            color: row.status ? palette.white : palette.neon,
                          }}
                          className="chip"
                          label={row.status ? 'Sin Habilitar' : 'Habilitado '}
                        /> */}
                          Pendiente
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {row.email}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {row.phone}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          Enviar Mailing
                        </TableCell>
                      </TableRow>
                    ))
                ) : (
                  <TableRow className="tableComponent__row">
                    <TableCell
                      colSpan={labels.length}
                      className="tableComponent__notFound "
                    >
                      😅 No hay entrevistas
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>
      <CustomModal handleClose={handleClose} open={open}>
        {/* <FormGeneralRegister handleClose={handleClose} type="Business" /> */}
      </CustomModal>
    </Styles>
  )
}
