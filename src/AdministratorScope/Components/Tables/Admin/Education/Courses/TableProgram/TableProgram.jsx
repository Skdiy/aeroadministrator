import React, { useState } from 'react'

import { Avatar, Chip } from '@mui/material'
import { Paper, Table, TableBody } from '@mui/material'
import { TableCell, TableContainer, TableHead, TableRow } from '@mui/material'
// Config
import { variables } from '../../../../../../config/variables'
// Components
import DateSelect from '../../../../../Global/DateSelect/DateSelect'
import SearchBasic from '../../../../../Global/SearchBasic/SearchBasic'
// Helper
import { labels } from './TableProgram.helper'
// Functions
import { handleFilter } from '../../../../../../Utils/functions/handleFilter'
// Styles
import Styles from './TableProgram.style'

const { palette } = variables

export default function TableProgam(props) {
  const { list } = props
  const [filterArray, setFilterArray] = useState('')

  const handleGetInput = (value) => {
    setFilterArray(value)
  }

  return (
    <Styles>
      <div className="header">
        {/* Filters */}
        <div className="header__wrapper">
          <p className="header__text">Programas activos</p>
          <p className="header__text">Programas dictados</p>
        </div>
      </div>
      <div className="header">
        <div className="selectDate">
          <DateSelect type="Mes" className="selectDate__separator" />
          <DateSelect type="Año" />
        </div>
        <SearchBasic label={'Buscar empresa'} handleGetInput={handleGetInput} />
      </div>

      <div className="containerTable">
        <Paper className="container">
          <TableContainer component={Paper} className="tableComponent">
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead className="tableComponent__head">
                <TableRow
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  className="tableComponent__row"
                >
                  {labels.map(({ name, key }) => (
                    <TableCell
                      className="tableComponent__text"
                      key={key.length ? key : name}
                    >
                      {name}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody className="tableComponent__body">
                {list.length ? (
                  list
                    /* Todo: Actualizar los campos con los obtenidos por QueryApi */
                    .filter((row) => handleFilter(row, filterArray))
                    ?.map((row, index) => (
                      <TableRow
                        key={index}
                        sx={{
                          '&:last-child td, &:last-child th': { border: 0 },
                        }}
                        className="tableComponent__row"
                      >
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          <div className="tableComponent__body__user">
                            <Avatar
                              alt={`${row.name}`}
                              src={
                                row.image ||
                                'https://mui.com/static/images/avatar/1.jpg'
                              }
                              className="tableComponent__body__user__avatar"
                            />
                            {row.name}
                          </div>
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: code, no se especifica en el backend*/}
                          {row.code || 'C12458'}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {row.level || 'Upgrade'}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {row.date_ini}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: N.Inscritos, no se especifica en el backend*/}
                          {row.qualification || '9'}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Capac.???, no se especifica en el backend*/}
                          {row.qualification || '20'}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Pendiente obtener name de instructor por id*/}
                          {'Garrido A.' || row.teacher_id}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Estado, no se especifica en el backend*/}
                          <Chip
                            sx={{
                              backgroundColor: row.status
                                ? palette.dark
                                : palette.white,
                              color: row.status ? palette.white : palette.neon,
                            }}
                            className="chip"
                            label={row.status ? 'Sin Habilitar' : 'Habilitado '}
                          />
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Details o description, no se especifica en el backend*/}
                          {row.description}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          {/*TODO: Legal , no se especifica en el backend*/}
                          {'Ver' || row.details}
                        </TableCell>
                        <TableCell className="tableComponent__text tableComponent__text--secondary">
                          <Chip
                            sx={{
                              backgroundColor: row.status
                                ? palette.palidBlueLight
                                : palette.dark,
                              color: palette.white,
                            }}
                            className="chip"
                            label={row.status ? 'Publicar' : 'Deshabilitar '}
                          />
                        </TableCell>
                      </TableRow>
                    ))
                ) : (
                  <TableRow className="tableComponent__row">
                    <TableCell
                      colSpan={labels.length}
                      className="tableComponent__notFound "
                    >
                      😅 No hay programas
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>
    </Styles>
  )
}
