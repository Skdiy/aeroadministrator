/* TODO: Colocar keys cuando esten listas */
export const labels = [
  { name: 'Course', key: '' },
  { name: 'Code', key: '' },
  { name: 'Category', key: '' },
  { name: 'Fecha de inicio', key: '' },
  { name: 'N.Inscritos', key: '' },
  { name: 'Capac.', key: '' },
  { name: 'Intructor', key: '' },
  { name: 'Estado', key: '' },
  { name: 'Details', key: '' },
  { name: 'Legal', key: '' },
  { name: '', key: '' },
]

export const dataFake = [
  {
    id: 1,
    name: 'Turbohélices',
    code: 'C12458',
    category: 'Upgrade',
    date_ini: 'Marzo 2021',
    n_inscritos: 10,
    capac: 10,
    teacher: 'Garrido A',
    state: ' Encurso',
  },
]
