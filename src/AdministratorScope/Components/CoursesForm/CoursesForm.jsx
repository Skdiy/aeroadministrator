import React from "react";

// Components
import SectionOne from "./Sections/SectionOne/SectionOne";
import SectionTwo from "./Sections/SectionTwo/SectionTwo";
import SectionThree from "./Sections/SectionThree/SectionThree";
import SectionFor from "./Sections/SectionFor/SectionFor";
import SectionFive from "./Sections/SectionFive/SectionFive";

// Styles
import Styles from "./CoursesForm.styles";

const CoursesForm = () => {
  return (
    <Styles className="CoursesForm">
      <SectionOne />
      <SectionTwo />
      <SectionThree />
      <SectionFor />
      <SectionFive />
    </Styles>
  );
};

export default CoursesForm;
