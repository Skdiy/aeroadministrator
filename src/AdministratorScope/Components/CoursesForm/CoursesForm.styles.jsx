import styled from "styled-components";
import { CONSTANTS } from "../../config/constants";
import { variables } from "../../config/variables";

const { BREAKPOINTS } = CONSTANTS;
const { palette, fonts } = variables;
const { tablet } = BREAKPOINTS;

const Styles = styled.div`
  color: white;
  .CoursesForm {
  }
`;

export default Styles;
