import React from "react";

// Styles
import Styles from "./TextField.styles";

const TextField = (props) => {
  const { onChange, value, rows } = props;
  const { label = "label", width, height } = props;
  const { type = "text", placeholder = "..." } = props;

  return (
    <Styles className="TextField" width={width} height={height}>
      <label htmlFor={label} className="TextField__label">
        {label}
      </label>
      <textarea
        type={type}
        name={label}
        id={label}
        value={value}
        onChange={onChange}
        className="TextField__input"
        placeholder={placeholder}
        rows={rows}
      />
    </Styles>
  );
};

export default TextField;
