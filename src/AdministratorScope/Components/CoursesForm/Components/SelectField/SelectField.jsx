import React from "react";

// Helpers
import { options } from "./SelectField.helper";
// Styles
import Styles from "./SelectField.styles";

const SelectField = (props) => {
  const { label = "label", width = "40%", height } = props;
  const { placeholder = "...", labels = options } = props;

  return (
    <Styles className="SelectField" width={width} height={height}>
      <label htmlFor={label} className="SelectField__label">
        {label}
      </label>
      <select
        name={label}
        className="SelectField__select SelectField__label"
        placeholder={placeholder}
      >
        {labels?.map((option, index) => (
          <option key={index} value={option}>
            {option}
          </option>
        ))}
      </select>
    </Styles>
  );
};

export default SelectField;
