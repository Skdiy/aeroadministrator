import React from "react";
import { useForm } from "react-hook-form";

// Components
import TextField from "../../Components/TextField/TextField";
import SelectField from "../../Components/SelectField/SelectField";
// Styles
import Styles from "./SectionOne.styles";

const SectionOne = () => {
  const { register, handleSubmit } = useForm();
  const {
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
    console.log(errors);
  };

  return (
    <Styles className="SectionOne">
      <h2 className="SectionOne__title">Qué vas a crear</h2>
      <form onSubmit={handleSubmit(onSubmit)}>
        <SelectField
          register={register}
          required={true}
          label="Categoría"
          placeholder="Categoría"
        />
        <SelectField
          register={register}
          required={true}
          label="Tipo"
          placeholder="Tipo"
        />
        <TextField
          register={register}
          required={true}
          onChange={() => {}}
          label="Nombre del curso"
          placeholder="Nombre del curso"
        />
        <TextField
          register={register}
          required={true}
          onChange={() => {}}
          label="Descrip. corta TO CARD"
          placeholder="Descrip. corta TO CARD"
          height="100px"
          rows="5"
        />
        <SelectField
          register={register}
          required={true}
          label="A cargo de"
          placeholder="A cargo de"
        />
      </form>
    </Styles>
  );
};

export default SectionOne;
