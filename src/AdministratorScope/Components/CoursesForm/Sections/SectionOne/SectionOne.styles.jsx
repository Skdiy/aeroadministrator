import styled from "styled-components";
import { CONSTANTS } from "../../../../config/constants";
import { variables } from "../../../../config/variables";

const { BREAKPOINTS } = CONSTANTS;
const { palette, fonts } = variables;
const { tablet } = BREAKPOINTS;

const Styles = styled.div`
  color: white;
  border-bottom: 1px solid ${palette.borderColor};
  margin-bottom: 72px;
  .SectionOne {
    &__title {
      font-size: 24px;
      line-height: 32px;
      font-weight: 800;
    }
  }
`;

export default Styles;
