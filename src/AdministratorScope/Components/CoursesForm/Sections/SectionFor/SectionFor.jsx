import React from "react";

// Styles
import Styles from "./SectionFor.styles";

const SectionFor = () => {
  return <Styles className="SectionFor">SectionFor</Styles>;
};

export default SectionFor;
