import React from 'react'
// Style
import Style from './Input.style'

export default function Input(props) {
  const { name, type, onChange, placeholder } = props
  return (
    <Style>
      <input
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        type={type}
      />
    </Style>
  )
}
