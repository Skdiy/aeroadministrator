import styled from 'styled-components'
import { CONSTANTS } from '../../../config/constants'
import { variables } from '../../../config/variables'

const { desktop } = CONSTANTS.BREAKPOINTS
const { palette, fonts, gradients } = variables

const Styles = styled.div`
  @media screen and (max-width: ${desktop}px) {
    display: contents;
  }

  .css-9ddj71-MuiInputBase-root-MuiOutlinedInput-root {
    height: 56px;
    width: 380px;
    border-radius: 5px;
    font-family: ${fonts['Nova-Lt']};
    background-color: ${palette.white};

    @media screen and (max-width: ${desktop}px) {
      width: auto;
    }
  }

  .css-1sumxir-MuiFormLabel-root-MuiInputLabel-root {
    background: ${gradients.second1};
    background-clip: text;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    color: ${palette.grSecond2.c2};
  }

  .css-9ddj71-MuiInputBase-root-MuiOutlinedInput-root.Mui-focused
    .MuiOutlinedInput-notchedOutline {
    border: none;
  }
`

export default Styles
