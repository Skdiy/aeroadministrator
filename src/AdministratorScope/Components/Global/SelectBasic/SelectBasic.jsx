import * as React from 'react'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
// Styles
import Styles from './SelectBasic.style'

export default function SelectBasic(props) {
  const { labels, handleGetFilter } = props
  const [label, setLabel] = React.useState('')

  const handleChange = (event) => {
    handleGetFilter(event.target.value)
    setLabel(event.target.value)
  }

  return (
    <Styles>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Ordenar Por</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={label}
          label="Age"
          onChange={handleChange}
        >
          {labels.map(({ name, key }) => (
            <MenuItem
              value={key.length ? key : name}
              key={key.length ? key : name}
            >
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Styles>
  )
}
