import React from 'react'

import { Avatar, Button } from '@mui/material'
import { Paper, Table, TableBody } from '@mui/material'
import { TableCell, TableContainer, TableHead, TableRow } from '@mui/material'
// Styles
import Styles from './TableCourseParticipantDetails.styles'

export default function TableCourseParticipantDetails(props) {
  const { labels, list } = props
  /* Todo: Actualizar con los datos traidos del endpoint */
  return (
    <Styles>
      <Paper>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                {labels.map((label) => (
                  <TableCell key={label}>{label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {list.length ? (
                list?.map((row, index) => (
                  <TableRow
                    key={index}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell className="textCell text">
                      <Avatar
                        alt={`${row.name}`}
                        src={
                          row.image ??
                          'https://mui.com/static/images/avatar/1.jpg'
                        }
                        className="iconSeparator"
                      />
                      {`${row.name}`}
                    </TableCell>
                    <TableCell className="textCell">{row.country}</TableCell>
                    <TableCell className="textCell">{row.email}</TableCell>
                    <TableCell className="textCell">{row.course}</TableCell>
                    <TableCell className="textCell">
                      {row.condition ? row.condition : '-'}
                    </TableCell>
                    <TableCell className="textCell">{row.state}</TableCell>
                    <TableCell className="textCell">
                      {row.progress} %{' '}
                    </TableCell>
                    <TableCell className="textCell">{`${row.asistencia}/${row.total_asistencia}`}</TableCell>
                    <TableCell className="textCell">{row.test_01}</TableCell>
                    <TableCell className="textCell">
                      {row.test_02 ? row.test_02 : '-'}
                    </TableCell>
                    <TableCell className="textCell">{row.test_02}</TableCell>
                    <TableCell className="textCell">{row.proyect}</TableCell>
                    <TableCell className="textCell">{row.prom}</TableCell>
                    <TableCell className="textCell">
                      {row.inasistencia}
                    </TableCell>
                    <TableCell className="textCell">{row.total}</TableCell>
                    <TableCell className="textCell">
                      <Button className="button">Ver Progreso</Button>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow className="tableComponent__row">
                  <TableCell
                    colSpan={labels.length}
                    className="tableComponent__notFound"
                  >
                    😅 No hay participantes
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Styles>
  )
}
