import styled from 'styled-components'

import { variables } from '../../../../../../config/variables'

const { palette, fonts } = variables

const Styles = styled.div`
  margin-top: 40px;

  .css-11xur9t-MuiPaper-root-MuiTableContainer-root {
    background-color: transparent;
    box-shadow: none;
    width: auto;
    font-family: ${fonts['Nova-Lt']};
  }

  .css-1ygcj2i-MuiTableCell-root {
    font-family: ${fonts['Nova-Lt']};
    color: ${palette.white};
    font-size: 12px;
    text-align: center;
    text-transform: capitalize;
  }

  .css-1ex1afd-MuiTableCell-root {
    font-family: ${fonts['Nova-Lt']};
    color: ${palette.white};
    font-size: 16px;
  }

  .textCell {
    text-align: center;
  }

  .text {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .iconSeparator {
    margin: 8px;
  }

  .button {
    font-family: ${fonts['Nova-Lt']};
    color: ${palette.white};
    padding: 6px 8px;
    border: 2px solid ${palette.palidBlueLight}
    white-space: nowrap;
    overflow: hidden;
    text-overflow: clip;
  }
`

export default Styles
