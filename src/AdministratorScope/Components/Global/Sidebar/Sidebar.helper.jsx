export const data = [
  {
    title: 'Education',
    id: 'education',
    icon: 1,
    links: [
      { name: 'Registro General', url: '/admin/general-register' },
      { name: 'Entrevista', url: '/admin/interviews' },
      { name: 'Cursos', url: '/admin/courses' },
      { name: 'Artículos', url: '/admin/articles' },
      { name: 'Livestream', url: '/admin/live-streams' },
      { name: 'Finanzas o Pagos', url: '/admin/finances' },
    ],
  },
  {
    title: 'Feria',
    id: 'feria',
    icon: 2,
    links: [
      { name: 'Registro General', url: '/feria/general-register' },
      { name: 'Finanzas o Pagos', url: '/feria/finanzas-pagos' },
    ],
  },
  {
    title: 'Business',
    id: 'business',
    icon: 3,
    links: [{ name: 'Registro General', url: '/bussiness/general-register' }],
  },
  {
    title: 'Maintenance',
    id: 'maintenance',
    icon: 4,
    links: [{ name: 'Registro General', url: '/maintenance/general-register' }],
  },
]
