import React from 'react'

import { Box, Modal } from '@mui/material'
// Styles
import { style } from './Modal.styles'

const CustomModal = (props) => {
  const { open, handleClose, children } = props

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>{children}</Box>
      </Modal>
    </div>
  )
}

export default CustomModal
