import { variables } from "../../../config/variables";

const { gradients } = variables;

export const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 752,
  borderRadius: 8,
  background: gradients.second2,
  p: 4,
  boxShadow: 24,
  zIndex: 800,
};
